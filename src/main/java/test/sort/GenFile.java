package test.sort;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;

public class GenFile {

    public static void generate(String name, long size) throws IOException {
        try (RandomAccessFile f = new RandomAccessFile(name, "rw"); FileChannel fc = f.getChannel()) {
            f.setLength(size);
            int bufSize = 8192;
            byte[] b = new byte[bufSize];
            Random random = new Random();
            long wr = 0;
            ByteBuffer byteBuffer = ByteBuffer.wrap(b);
            while (wr < size) {
                random.nextBytes(b);
                byteBuffer.limit((int) Math.min(bufSize, size - wr));
                wr += fc.write(byteBuffer);
                byteBuffer.rewind();
            }
        }
    }

    public static void generate(String name, int... values) throws IOException {
        try (RandomAccessFile f = new RandomAccessFile(name, "rw"); FileChannel fc = f.getChannel()) {
            ByteBuffer b = ByteBuffer.allocate(values.length * SortFile.INT_SIZE);
            b.asIntBuffer().put(values);
            fc.write(b);
        }
    }
}
