package test.sort;

import test.sort.data.FileData;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class SortFile {
    public static final int DEFAULT_MAX_BUF_SIZE = 128 * 1024 * 1024;
    public static final int  INT_SIZE = 4;
    private static final String tmpFileName = "tmp_sort_file";

    public static void sort(String src, String dest, int threadsN,
                            int maxBufSize) throws IOException {
        try (RandomAccessFile f1 = new RandomAccessFile(src, "rw");
             RandomAccessFile copy1 = new RandomAccessFile(dest, "rw");
             RandomAccessFile copy2 = new RandomAccessFile(tmpFileName, "rw");
             FileChannel srcFc = f1.getChannel();
             FileChannel copy1Fc = copy1.getChannel();
             FileChannel copy2Fc = copy2.getChannel()
        ) {
            copy1.setLength(f1.length());
            copy2.setLength(f1.length());

            ForkJoinPool pool = new ForkJoinPool(threadsN);
            int threshold = (int) Runtime.getRuntime().maxMemory() / (threadsN * INT_SIZE * 2);
            int nSplits = 2 * Runtime.getRuntime().availableProcessors();
            FileData srcData = new FileData(srcFc, maxBufSize);
            FileData copy1Data = new FileData(copy1Fc, maxBufSize);
            FileData copy2Data = new FileData(copy2Fc, maxBufSize);
            ForkJoinTask<Void> task = new MergeSort(srcData, copy1Data, copy2Data,
                    0, f1.length() / INT_SIZE, threshold, nSplits);
            pool.submit(task);
            task.join();

            if (task.isCompletedAbnormally() && task.getException() != null) {
                System.err.println("Failed to sort file " + src + ": " + task.getException().getMessage());
            }
        } finally {
            Files.delete(Path.of(tmpFileName));
        }
    }

    public static void sort(String src, String dest, int threadsN) throws IOException {
        sort(src, dest, threadsN, DEFAULT_MAX_BUF_SIZE);
    }
}
