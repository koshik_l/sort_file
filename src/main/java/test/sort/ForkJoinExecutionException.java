package test.sort;

public class ForkJoinExecutionException extends RuntimeException {
    public ForkJoinExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
