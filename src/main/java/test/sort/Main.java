package test.sort;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.out.println("wrong number of arguments: expected 3, got " + args.length);
            return;
        }
        int nThreads = Integer.parseInt(args[0]);
        String src = args[1];
        String dst = args[2];

        SortFile.sort(src, dst, nThreads);
    }
}
