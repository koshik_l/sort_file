package test.sort;

import test.sort.data.ArrayData;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class SortArray {
    private static final int DEFAULT_THRESHOLD = 1;

    public static int[] sort(int[] array, int nThreads, int threshold) {
        int[] copy1 = new int[array.length];
        int[] copy2 = new int[array.length];
        ForkJoinPool pool = new ForkJoinPool(nThreads);
        ForkJoinTask<Void> task = new MergeSort(new ArrayData(array), new ArrayData(copy1), new ArrayData(copy2),
                0, array.length, threshold, 2);
        pool.submit(task);
        task.join();
        return copy1;
    }

    public static int[] sort(int[] array, int nThreads) {
        return sort(array, nThreads, DEFAULT_THRESHOLD);
    }
}
