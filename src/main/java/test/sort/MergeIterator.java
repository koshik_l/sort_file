package test.sort;

import test.sort.data.DataReader;

import java.io.IOException;
import java.util.*;

public class MergeIterator {
    private final Queue<Node> queue;

    public MergeIterator(List<DataReader> sources) throws IOException {
         queue = new PriorityQueue<>(Comparator.comparingInt(n -> n.curValue));

         for (DataReader s : sources) {
             if (s.hasElements()) {
                 Node n = new Node(s, s.get());
                 queue.add(n);
             }
         }
    }

    public boolean hasNext() {
        return !queue.isEmpty();
    }

    public int nextMin() throws IOException {
        if (queue.isEmpty()) throw new NoSuchElementException();

        Node m = queue.poll();
        int min = m.curValue;
        if (m.reader.hasElements()) {
            m.curValue = m.reader.get();
            queue.add(m);
        }
        return min;
    }

    private static class Node {
        private final DataReader reader;
        private int curValue;

        public Node(DataReader reader, int curValue) {
            this.reader = reader;
            this.curValue = curValue;
        }
    }
}
