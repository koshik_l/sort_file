package test.sort;

import test.sort.data.Data;
import test.sort.data.DataReader;
import test.sort.data.DataWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class MergeSort extends RecursiveAction {
    private final Data src;
    private final Data copy1;
    private final Data copy2;
    private final long low;
    private final long high;
    private final int threshold;
    private final int nSplits;

    public MergeSort(Data src, Data copy1, Data copy2, long low, long high, int threshold, int nSplits) {
        this.src = src;
        this.copy1 = copy1;
        this.copy2 = copy2;
        this.low = low;
        this.high = high;
        this.threshold = threshold;
        this.nSplits = nSplits;
    }

    @Override
    protected void compute() {
        try {
            if (high - low <= threshold) {
                int[] a = src.get(low, high);
                Arrays.sort(a);
                copy1.set(low, a);
            } else {
                List<MergeSort> sortTasks = sortTasks();
                invokeAll(sortTasks);
                for (MergeSort t : sortTasks) {
                    if (t.isCompletedAbnormally() && t.getException() != null) {
                        throw new ForkJoinExecutionException(t.getException().getMessage(), t.getException());
                    }
                }
                merge();
            }
        } catch (IOException e) {
            throw new ForkJoinExecutionException(e.getMessage(), e);
        }
    }

    private void merge() throws IOException {
        DataWriter merged = copy1.writer(low, high);

        MergeIterator it = new MergeIterator(readers(copy2));
        while (it.hasNext())
            merged.add(it.nextMin());
    }

    private long[] intervals() {
        long intervalSize = Math.max((high - low) / nSplits, threshold);
        int nItervals = 1 + (int) ((high - low - 1) / intervalSize);
        long[] r = new long[nItervals + 1];
        long cLow = low;
        for (int i = 0; i < nItervals; i++) {
            r[i] = cLow;
            cLow += intervalSize;
        }
        r[nItervals] = high;

        return r;
    }

    private List<DataReader> readers(Data source) {
        long[] intervals = intervals();
        List<DataReader> readers = new ArrayList<>(intervals.length);
        for (int i = 0; i < intervals.length - 1; i++) {
            readers.add(source.reader(intervals[i], intervals[i + 1]));
        }
        return readers;
    }

    private List<MergeSort> sortTasks() {
        long[] intervals = intervals();
        List<MergeSort> tasks = new ArrayList<>(intervals.length);
        for (int i = 0; i < intervals.length - 1; i++) {
            tasks.add(new MergeSort(src, copy2, copy1, intervals[i], intervals[i + 1], threshold, nSplits));
        }
        return tasks;
    }
}
