package test.sort.data;

import java.io.IOException;

public abstract class DataReader {
    protected final long to;
    protected long cur;

    public DataReader(long from, long to) {
        this.to = to;
        cur = from;
    }

    public abstract int get() throws IOException;
    public boolean hasElements() {
        return cur < to;
    }
}
