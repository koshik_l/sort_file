package test.sort.data;

import test.sort.SortFile;

import java.io.IOException;
import java.nio.IntBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileData implements Data {
    private final FileChannel fileChannel;
    private final int maxBufSize;

    public FileData(FileChannel fileChannel, int maxBufSize) {
        this.fileChannel = fileChannel;
        this.maxBufSize = maxBufSize;
    }

    @Override
    public int[] get(long low, long high) throws IOException {
        MappedByteBuffer mb = fileChannel.map(FileChannel.MapMode.READ_ONLY, low * SortFile.INT_SIZE,
                (high - low) * SortFile.INT_SIZE);
        int[] r = new int[(int) (high - low)];
        mb.asIntBuffer().get(r);
        return r;
    }

    @Override
    public void set(long low, int[] d) throws IOException {
        MappedByteBuffer mb = fileChannel.map(FileChannel.MapMode.READ_WRITE, low * SortFile.INT_SIZE,
                d.length * SortFile.INT_SIZE);
        IntBuffer intBuffer = mb.asIntBuffer();
        intBuffer.put(d);
    }

    @Override
    public DataReader reader(long from, long to) {
        return new FileDataReader(from, to, maxBufSize, fileChannel);
    }

    @Override
    public DataWriter writer(long from, long to) {
        return new FileDataWriter(from, to, maxBufSize, fileChannel);
    }
}
