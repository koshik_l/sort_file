package test.sort.data;

import test.sort.SortFile;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileDataWriter extends DataWriter {
    private final FileChannel fileChannel;
    private final int maxBufSize;
    private MappedByteBuffer buffer;

    public FileDataWriter(long from, long to, int maxBufSize, FileChannel fileChannel) {
        super(from, to);
        this.fileChannel = fileChannel;
        this.maxBufSize = maxBufSize;
    }

    @Override
    public void add(int value) throws IOException {
        if (buffer == null)
            remap();

        buffer.putInt(value);
        cur++;

        if (buffer.position() >= buffer.limit())
            remap();
    }

    private void remap() throws IOException {
        int bs = (int) Math.min(maxBufSize, (to - cur) * SortFile.INT_SIZE);
        if (bs == 0) return;
        buffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, cur * SortFile.INT_SIZE, bs);
    }
}
