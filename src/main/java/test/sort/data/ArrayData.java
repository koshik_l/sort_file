package test.sort.data;

import java.util.Arrays;

public class ArrayData implements Data {
    private final int[] array;

    public ArrayData(int[] array) {
        this.array = array;
    }

    @Override
    public int[] get(long low, long high) {
        return Arrays.copyOfRange(array, (int)low, (int)high);
    }

    @Override
    public void set(long low, int[] d) {
        System.arraycopy(d, 0, array, (int)low, d.length);
    }

    @Override
    public DataReader reader(long from, long to) {
        return new ArrayDataReader(from, to, array);
    }

    @Override
    public DataWriter writer(long from, long to) {
        return new ArrayDataWriter(from, to, array);
    }
}
