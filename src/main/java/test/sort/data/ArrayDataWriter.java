package test.sort.data;

public class ArrayDataWriter extends DataWriter {
    private final int[] arr;

    public ArrayDataWriter(long from, long to, int[] arr) {
        super(from, to);
        this.arr = arr;
    }

    @Override
    public void add(int value) {
        arr[(int)cur] = value;
        cur++;
    }
}
