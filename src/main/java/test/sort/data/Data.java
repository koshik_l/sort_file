package test.sort.data;

import java.io.IOException;

public interface Data {
    int[] get(long low, long high) throws IOException;
    void set(long low, int[] d) throws IOException;

    DataReader reader(long from, long to);
    DataWriter writer(long from, long to);
}
