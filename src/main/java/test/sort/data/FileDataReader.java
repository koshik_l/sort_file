package test.sort.data;

import test.sort.SortFile;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileDataReader extends DataReader {
    private final FileChannel fileChannel;
    private final int maxBufSize;
    private MappedByteBuffer buffer;

    public FileDataReader(long from, long to, int maxBufSize, FileChannel fc) {
        super(from, to);
        this.maxBufSize = maxBufSize;
        this.fileChannel = fc;
    }

    @Override
    public int get() throws IOException {
        checkRead();
        int value = buffer.getInt();
        cur++;
        return value;
    }

    private void checkRead() throws IOException {
        if (buffer == null ||!buffer.hasRemaining()) {
            int bs = (int) Math.min(maxBufSize, (to - cur) * SortFile.INT_SIZE);
            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, cur * SortFile.INT_SIZE, bs);
        }
    }
}
