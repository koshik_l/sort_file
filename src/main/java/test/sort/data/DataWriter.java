package test.sort.data;

import java.io.IOException;

public abstract class DataWriter {
    protected final long to;
    protected long cur;

    public DataWriter(long from, long to) {
        this.to = to;
        cur = from;
    }

    public abstract void add(int value) throws IOException;
}
