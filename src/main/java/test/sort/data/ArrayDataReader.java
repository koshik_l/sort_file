package test.sort.data;

public class ArrayDataReader extends DataReader {
    private final int[] arr;

    public ArrayDataReader(long from, long to, int[] arr) {
        super(from, to);
        this.arr = arr;
    }

    @Override
    public int get() {
        int value = arr[(int) cur];
        cur++;
        return value;
    }
}
