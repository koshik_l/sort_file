package test.sort;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class SortFileTest {
    private final String fileName = "tmp_file";
    private final String destFileName = "dst";

    @TempDir
    public Path tempDir;

    @Test
    void sort_empty() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString());

        SortFile.sort(file.toString(), destFile.toString(), 1, 4);

        RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
        assertEquals(0, f.length());
        f.close();
    }

    @Test
    void sort_oneElement() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), new int[]{-100});

        SortFile.sort(file.toString(), destFile.toString(), 1, 4);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
        FileChannel fc = f.getChannel()) {
            assertEquals(4, f.length());
            int[] value = read(fc, 1);
            assertEquals(-100, value[0]);
        }
    }

    @Test
    void sort_descending() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), 4, 2, 1, 0, -35, -565);

        SortFile.sort(file.toString(), destFile.toString(), 1, 8);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
            FileChannel fc = f.getChannel()) {
            assertEquals(24, f.length());
            int[] values = read(fc, 6);
            assertArrayEquals(new int[]{-565, -35, 0, 1, 2, 4}, values);
        }
    }

    @Test
    void sort_sorted() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), -300, -34, -2, 0, 2, 356, 3452);

        SortFile.sort(file.toString(), destFile.toString(), 1, 8);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
            FileChannel fc = f.getChannel()) {
            assertEquals(28, f.length());
            int[] values = read(fc, 7);
            assertArrayEquals(new int[]{-300, -34, -2, 0, 2, 356, 3452}, values);
        }
    }

    @Test
    void sort_nonMultThreshold() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), 5, 7, 1, 9, 4, 7, 3, 3, 1);

        SortFile.sort(file.toString(), destFile.toString(), 1, 8);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
            FileChannel fc = f.getChannel()) {
            assertEquals(36, f.length());
            int[] values = read(fc, 9);
            assertArrayEquals(new int[]{1, 1, 3, 3, 4, 5, 7, 7, 9}, values);
        }
    }

    @Test
    void sort_parallel() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), 5, 7, 1, 9, 4, 7, 3, 3, 1);

        SortFile.sort(file.toString(), destFile.toString(), 4, 8);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
            FileChannel fc = f.getChannel()) {
            assertEquals(36, f.length());
            int[] values = read(fc, 9);
            assertArrayEquals(new int[]{1, 1, 3, 3, 4, 5, 7, 7, 9}, values);
        }
    }

    @Test
    void sort_bufSizeGreaterThanFile() throws IOException {
        Path file = tempDir.resolve(fileName);
        Path destFile = tempDir.resolve(destFileName);
        GenFile.generate(file.toString(), 5, 7, 1, 9, 4, 7, 3, 3, 1);

        SortFile.sort(file.toString(), destFile.toString(), 4, 40);

        try(RandomAccessFile f = new RandomAccessFile(destFile.toString(), "r");
            FileChannel fc = f.getChannel()) {
            assertEquals(36, f.length());
            int[] values = read(fc, 9);
            assertArrayEquals(new int[]{1, 1, 3, 3, 4, 5, 7, 7, 9}, values);
        }
    }

    private int[] read(FileChannel fc, int n) throws IOException {
        int[] r = new int[n];
        ByteBuffer byteBuffer = ByteBuffer.allocate(n * 4);
        fc.read(byteBuffer);
        byteBuffer.rewind();
        byteBuffer.asIntBuffer().get(r);

        return r;
    }
}
