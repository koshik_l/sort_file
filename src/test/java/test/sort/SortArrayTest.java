package test.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortArrayTest {

    @Test
    void sort_empty() {
        int[] a = new int[]{};

        int[] r = SortArray.sort(a, 1);

        assertArrayEquals(new int[]{}, r);
    }

    @Test
    void sort_oneElement() {
        int[] a = new int[]{6};

        int[] r = SortArray.sort(a, 1);

        assertArrayEquals(a, r);
    }

    @Test
    void sort_descending() {
        int[] a = new int[]{7, 6, 5, 4, 3, 2, 1, -7};

        int[] r = SortArray.sort(a, 1);

        assertArrayEquals(new int[]{-7, 1, 2, 3, 4, 5, 6, 7}, r);
    }

    @Test
    void sort_sorted() {
        int[] a = new int[]{1, 2, 3, 3, 5};

        int[] r = SortArray.sort(a, 1);

        assertArrayEquals(a, r);
    }

    @Test
    void sort_nonMultThreshold() {
        int[] a = new int[]{5, 7, 1, 9, 4, 7, 3, 3, 1};

        int[] r = SortArray.sort(a, 1, 5);

        assertArrayEquals(new int[]{1, 1, 3, 3, 4, 5, 7, 7, 9}, r);
    }

    @Test
    void sort_Parallel() {
        int[] a = new int[]{5, 7, 1, 9, 4, 7, 3, 3, 1};

        int[] r = SortArray.sort(a, 3, 2);

        assertArrayEquals(new int[]{1, 1, 3, 3, 4, 5, 7, 7, 9}, r);
    }
}
