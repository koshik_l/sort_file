package test.sort;

import org.junit.jupiter.api.Test;
import test.sort.data.DataReader;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MergeIteratorTest {

    @Test
    void hasNext_zeroSources() throws IOException {
        MergeIterator mergeIterator = new MergeIterator(List.of());

        assertFalse(mergeIterator.hasNext());
    }

    @Test
    void hasNext_emptySources() throws IOException {
        DataReader reader1 = mock(DataReader.class);
        when(reader1.hasElements()).thenReturn(false);
        DataReader reader2 = mock(DataReader.class);
        when(reader2.hasElements()).thenReturn(false);
        MergeIterator it = new MergeIterator(List.of(reader1, reader2));

        assertFalse(it::hasNext);
    }

    @Test
    void hasNext_nonEmptySources() throws IOException {
        DataReader reader1 = mock(DataReader.class);
        when(reader1.get()).thenReturn(1);
        when(reader1.hasElements()).thenReturn(true, false);
        DataReader reader2 = mock(DataReader.class);
        when(reader2.get()).thenReturn(2, 7);
        when(reader2.hasElements()).thenReturn(true, true, false);
        MergeIterator it = new MergeIterator(List.of(reader1, reader2));

        assertTrue(it.hasNext());
    }

    @Test
    void nextMin_singleSource() throws IOException {
        DataReader reader = mock(DataReader.class);
        when(reader.get()).thenReturn(1, 2, 2, 5, 8, 9);
        when(reader.hasElements()).thenReturn(true, true, true, true, true, true, false);
        MergeIterator it = new MergeIterator(List.of(reader));
        int[] expected = new int[]{1, 2, 2, 5, 8, 9};
        int[] result = new int[expected.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = it.nextMin();
        }

        assertArrayEquals(expected, result);
    }

    @Test
    void nextMin_severalSources() throws IOException {
        DataReader reader1 = mock(DataReader.class);
        when(reader1.get()).thenReturn(1, 4);
        when(reader1.hasElements()).thenReturn(true, true, false);
        DataReader reader2 = mock(DataReader.class);
        when(reader2.get()).thenReturn(2, 7);
        when(reader2.hasElements()).thenReturn(true, true, false);
        DataReader reader3 = mock(DataReader.class);
        when(reader3.get()).thenReturn(4, 9);
        when(reader3.hasElements()).thenReturn(true, true, false);
        MergeIterator it = new MergeIterator(List.of(reader1, reader2, reader3));
        int[] expected = new int[]{1, 2, 4, 4, 7, 9};
        int[] result = new int[expected.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = it.nextMin();
        }

        assertArrayEquals(expected, result);
    }

    @Test
    void nextMin_emptySources() throws IOException {
        DataReader reader1 = mock(DataReader.class);
        when(reader1.hasElements()).thenReturn(false);
        DataReader reader2 = mock(DataReader.class);
        when(reader2.hasElements()).thenReturn(false);
        DataReader reader3 = mock(DataReader.class);
        when(reader3.hasElements()).thenReturn( false);
        MergeIterator it = new MergeIterator(List.of(reader1, reader2, reader3));

        assertThrows(NoSuchElementException.class, it::nextMin);
    }
}
