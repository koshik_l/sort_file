package test.sort.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import test.sort.GenFile;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileDataReaderTest {
    private final String fileName = "tmp_file";

    @TempDir
    public Path tempDir;

    @Test
    void get() throws IOException {
        Path file = tempDir.resolve(fileName);
        GenFile.generate(file.toString(), -1, 2, 3, 4, 5);
        try (RandomAccessFile f = new RandomAccessFile(file.toString(), "r");
             FileChannel fc = f.getChannel()) {
            FileDataReader reader = new FileDataReader(0, 5, 8, fc);
            int[] res = new int[5];
            for (int i = 0; i < res.length; i++)
                res[i] = reader.get();

            assertArrayEquals(new int[]{-1, 2, 3, 4, 5}, res);
        }
    }
}
