package test.sort.data;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import test.sort.GenFile;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileDataWriterTest {
    private final String fileName = "tmp_file";

    @TempDir
    public Path tempDir;

    @Test
    void add_addedExtraValues() throws IOException {
        Path file = tempDir.resolve(fileName);
        GenFile.generate(file.toString(), 0, 2, 3, 4);
        try (RandomAccessFile f = new RandomAccessFile(file.toString(), "rw");
             FileChannel fc = f.getChannel()) {
            FileDataWriter reader = new FileDataWriter(1, 2, 8, fc);

            reader.add(10);
            assertThrows(BufferOverflowException.class, () -> reader.add(20));
        }
    }

    @Test
    void add() throws IOException {
        Path file = tempDir.resolve(fileName);
        GenFile.generate(file.toString(), 0, 2, 3, 4);
        try (RandomAccessFile f = new RandomAccessFile(file.toString(), "rw");
             FileChannel fc = f.getChannel()) {
            FileDataWriter reader = new FileDataWriter(1, 3, 8, fc);

            reader.add(-10);
            reader.add(-20);

            ByteBuffer b = ByteBuffer.allocate(16);
            fc.read(b);
            b.rewind();
            int[] fileC = new int[4];
            b.asIntBuffer().get(fileC);
            assertArrayEquals(new int[]{0, -10, -20, 4}, fileC);
        }
    }
}