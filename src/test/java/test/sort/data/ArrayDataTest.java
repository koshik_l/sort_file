package test.sort.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayDataTest {

    @Test
    void get_emptySubarray() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);
        int[] expected = new int[0];

        int[] r = data.get(1, 1);

        assertArrayEquals(expected, r);
    }

    @Test
    void get() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);
        int[] expected = new int[]{2, 3};

        int[] r = data.get(1, 3);

        assertArrayEquals(expected, r);
    }

    @Test
    void get_outOfBoundsRange() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);
        int[] expected = new int[]{2, 3, 0, 0, 0};

        int[] r = data.get(1, 6);

        assertArrayEquals(expected, r);
    }

    @Test
    void get_negativeRange() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);

        assertThrows(IndexOutOfBoundsException.class, () -> data.get(-1, 6));
    }

    @Test
    void set_emptySubarray() {
        int[] a = new int[]{1, 2, 3};
        int[] expected = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);

        data.set(0, new int[0]);

        assertArrayEquals(a, expected);
    }

    @Test
    void set() {
        int[] a = new int[]{1, 2, 3};
        int[] expected = new int[]{5, 6, 3};
        ArrayData data = new ArrayData(a);

        data.set(0, new int[]{5, 6});

        assertArrayEquals(a, expected);
    }

    @Test
    void set_largeSubarray() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);

        assertThrows(IndexOutOfBoundsException.class, () -> data.set(2, new int[]{5, 6, 7, 8}));
    }

    @Test
    void set_negativeRange() {
        int[] a = new int[]{1, 2, 3};
        ArrayData data = new ArrayData(a);

        assertThrows(IndexOutOfBoundsException.class, () -> data.set(-2, new int[]{5, 6}));
    }
}
