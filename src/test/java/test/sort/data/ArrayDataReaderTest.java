package test.sort.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayDataReaderTest {

    @Test
    void get() {
        int[] a = new int[]{1, 2, 3, 4, 5};
        ArrayDataReader reader = new ArrayDataReader(2, 4, a);
        int[] expected = new int[]{3, 4};
        int[] actual = new int[2];

        for (int i = 0; i < actual.length; i++)
            actual[i] = reader.get();


        assertArrayEquals(expected, actual);
    }
}
