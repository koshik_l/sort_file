package test.sort.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayDataWriterTest {

    @Test
    public void add() {
        int[] a = new int[]{1, 2, 3};
        ArrayDataWriter writer = new ArrayDataWriter(0, 2, a);
        int[] expected = new int[]{5, 8, 9};

        writer.add(5);
        writer.add(8);
        writer.add(9);

        assertArrayEquals(expected, a);
    }

    @Test
    public void add_ExtraValues() {
        int[] a = new int[]{1, 2, 3};
        ArrayDataWriter writer = new ArrayDataWriter(0, 1, a);
        int[] expected = new int[]{5, 8, 9};

        writer.add(5);
        writer.add(8);
        writer.add(9);

        assertArrayEquals(expected, a);
    }
    @Test
    public void add_outOfBounds() {
        int[] a = new int[]{1, 2, 3};
        ArrayDataWriter writer = new ArrayDataWriter(3, 4, a);

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> writer.add(5));
    }
}
