package test.sort.data;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import test.sort.GenFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileDataTest {
    private final String fileName = "tmp_file";

    @TempDir
    public Path tempDir;

    @BeforeEach
    void setUp() throws IOException {
        Path file = tempDir.resolve(fileName);
        GenFile.generate(file.toString(), -1, 0, 4, -2, 5);
    }

    @Test
    void get() throws IOException {
        Path file = tempDir.resolve(fileName);
        try (RandomAccessFile f = new RandomAccessFile(file.toString(), "r");
             FileChannel fc = f.getChannel()) {
            FileData fileData = new FileData(fc, 0);
            int[] res = fileData.get(0, 5);

            assertArrayEquals(new int[]{-1, 0, 4, -2, 5}, res);
        }
    }

    @Test
    void set() throws IOException {
        Path file = tempDir.resolve(fileName);
        try (RandomAccessFile f = new RandomAccessFile(file.toString(), "rw");
             FileChannel fc = f.getChannel()) {
            FileData fileData = new FileData(fc, 0);
            fileData.set(1, new int[]{2, 6, 7});
            ByteBuffer byteBuffer = ByteBuffer.allocate(20);
            fc.read(byteBuffer);
            int[] res = new int[5];
            byteBuffer.rewind();
            byteBuffer.asIntBuffer().get(res);

            assertArrayEquals(new int[]{-1, 2, 6, 7, 5}, res);
        }
    }
}