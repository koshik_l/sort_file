Tested on Linux, openjdk13, i7-8565U processor, 8Gb RAM, SSD.

 
Decreasing of performance on number of threads > 8 can be explained by the following:
    
* number of threads of the processor is 8, so having paramenter nThreads > 8 would cause the slow down because of the context switching
* the size of an array sorted in memory inversely depends on the number of threads.

Results for 100Mb, 1Mb, 100kB files does not differ since they are sorted in memory.  


| File size     | # of threads  | Duration(sec) |
| ------------- | ------------- | ------------- |
| 16Gb          | 32            | 368           | 
| 16Gb          | 16            | 331           | 
| 16Gb          | 8             | 328           | 
| 16Gb          | 4             | 353           | 
| 16Gb          | 2             | 430           | 
| 16Gb          | 1             | 509           | 
| 4Gb           | 32            | 130           | 
| 4Gb           | 16            | 126           | 
| 4Gb           | 8             | 123           |
| 4Gb           | 4             | 144           |
| 4Gb           | 2             | 145           |
| 4Gb           | 1             | 196           |
| 1Gb           | 32            | 29            |
| 1Gb           | 16            | 27            |
| 1Gb           | 8             | 24            |
| 1Gb           | 4             | 24            |
| 1Gb           | 2             | 29            |
| 1Gb           | 1             | 41            |
| 100Mb         | 32            | 2             |
| 100Mb         | 16            | 2             |
| 100Mb         | 8             | 3             |
| 100Mb         | 4             | 3             |
| 100Mb         | 2             | 3             |
| 100Mb         | 1             | 3             |
| 1Mb           | 32            | 0             |
| 1Mb           | 16            | 0             |
| 1Mb           | 8             | 0             |
| 1Mb           | 4             | 0             |
| 1Mb           | 2             | 0             |
| 1Mb           | 1             | 0             |
| 100kB         | 32            | 0             |
| 100kB         | 16            | 0             |
| 100kB         | 8             | 0             |
| 100kB         | 4             | 0             |
| 100kB         | 2             | 0             |
| 100kB         | 1             | 0             |
